#! /usr/bin/python3

import gi
gi.require_version('Gtk', '3.0')
gi.require_version('Gst', '1.0')
from gi.repository import Gtk, Gst

import subprocess
import time

class Application:
  def __init__(self):
    Gst.init(None)
    self.setup_device()
    self.create_pipeline()
    self.create_ui()

  def setup_device(self):
    # Forward TCP port for RTSP device stream
    subprocess.check_call('mldb forward --remove-all', shell=True)
    subprocess.check_call('mldb forward tcp:8555 tcp:8555', shell=True)

    # Forward TCP port for HLS playback
    subprocess.check_call('mldb reverse --remove-all', shell=True)
    subprocess.check_call('mldb reverse tcp:8080 tcp:8080:xilinx-zcu106-revf',
        shell=True)

    # Ensure the app is not running. If it succeeds, that means the app was
    # running and is being killed. If it fails, it means the app was not
    # running.
    try:
      subprocess.check_call('mldb terminate -f com.collabora.gstreamer_demo',
          shell=True)
      print("Waiting 10s to be sure app terminated...")
      time.sleep(10)
    except subprocess.CalledProcessError:
      pass

    # Launch the app and give some time streams to start
    subprocess.check_call('mldb launch com.collabora.gstreamer_demo',
        shell=True)
    print("Waiting 10s for app to startup...")
    time.sleep(10)

    # Create an access token for the RTSP device stream. It's valid only for 10s
    try:
      token = subprocess.check_output('mldb rtsp-token 127.0.0.1', shell=True)
    except subprocess.CalledProcessError:
      print("Failed to get an RTSP token. Wait for 1min before retrying.")
      exit(1)

    # Start the RTSP server
    cmd = 'mldb launch com.collabora.gstreamer_demo -i ' + token.decode()
    subprocess.check_call(cmd, shell=True)
    time.sleep(5)

  def create_pipeline(self):
    videosink = Gst.ElementFactory.make('gtkglsink', None)
    sinkbin = Gst.ElementFactory.make('glsinkbin', None)
    sinkbin.set_property('sink', videosink)
    self.playbin = Gst.ElementFactory.make('playbin', None)
    self.playbin.set_property('video-sink', sinkbin)
    self.playbin.set_property('uri', 'rtspt://localhost:8555/ml1')
    self.playbin.connect('source-setup', self.source_setup_cb)
    self.video = videosink.get_property('widget')

  def source_setup_cb(self, playbin, source):
    source.set_property('latency', 200)

  def create_ui(self):
    grid = Gtk.Grid.new()
    grid.show()

    banner = Gtk.Image.new_from_file('ibc-banner.png')
    grid.attach(banner, 0, 0, 1, 1)
    banner.show()

    grid.attach(self.video, 1, 0, 1, 1)
    self.video.show()

    win = Gtk.Window()
    win.connect('destroy', Gtk.main_quit)
    win.resize(1280, 720)
    win.fullscreen()
    win.add(grid)
    win.show()

  def run(self):
    print("All done. You have to accept the confirmation dialog on device.")
    self.playbin.set_state(Gst.State.PLAYING)
    Gtk.main()

if __name__ == '__main__':
  app = Application()
  app.run()
